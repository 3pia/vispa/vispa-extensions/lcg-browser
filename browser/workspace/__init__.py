# -*- coding: utf-8 -*-

import os
import sys
import logging
import json
import time
from uuid import uuid4
from subprocess import Popen, PIPE
from threading import Thread
import shlex
import vispa

logger = logging.getLogger(__name__)


class LCGBrowserRpc:

    LCG_ROOT_VAR = "$LCG_ROOT"

    def __init__(self, nprocs=1, maxtime=30.):
        logger.debug("LCGBrowserRpc created")

        self.maxtime = maxtime

        self.queue = []
        self.pool  = Pool(nprocs=nprocs, maxtime=maxtime)

    @staticmethod
    def join(elem1, elem2):
        if elem1.endswith("/"):
            elem1 = elem1[:-1]
        if not elem2.startswith("/"):
            elem2 = "/" + elem2
        return elem1 + elem2

    @staticmethod
    def trim(cutoff, path):
        if path.startswith(cutoff):
            path = path[len(cutoff):]
        if not path.startswith("/"):
            path = "/" + path
        return path

    @staticmethod
    def expand(path):
        return os.path.expanduser(os.path.expandvars(path))

    def get_job(self, id, deep=False):
        lookup = self.queue[:]
        while len(lookup):
            job = lookup.pop(0)
            if job.id == id:
                return job
            elif deep and isinstance(job, JobGroup):
                lookup += job.jobs
        return None

    def remove(self, ids):
        encoded = isinstance(ids, (str, unicode))

        ids     = json.loads(ids) if encoded else ids
        removed = []

        valid_states = [AbstractJob.IDLE, AbstractJob.FINISHED]

        for id in ids:
            job = self.get_job(id, deep=True)
            if isinstance(job, JobGroup):
                if job.status in valid_states:
                    self.queue.remove(job)
                else:
                    continue
            elif isinstance(job, Job):
                # in the queue or in a jobgroup?
                if job.jobgroup is None:
                    if job.status in valid_states:
                        self.queue.remove(job)
                    else:
                        continue
                else:
                    if not job.jobgroup.remove(job):
                        continue
            else:
                continue

            removed.append(job.id)

        return json.dumps(removed) if encoded else removed

    def run(self, ids):
        encoded = isinstance(ids, (str, unicode))

        ids     = json.loads(ids) if encoded else ids
        started = []

        for id in ids:
            job = self.get_job(id)
            if not self.pool.add(job):
                continue
            started.append(id)

        return json.dumps(started) if encoded else started

    def terminate(self, ids):
        encoded = isinstance(ids, (str, unicode))

        ids = json.loads(ids) if encoded else ids

        for id in ids:
            job = self.get_job(id, deep=True)
            if isinstance(job, AbstractJob):
                job.terminate(reason="aborted by user")

    def get_queue(self):
        return json.dumps([job.__dict__ for job in self.queue])

    def lcgls(self, dsts, cmd, lcg_root, lcg_cutoff, grouped, nprocs, user_id, topic, maxtime=None):
        maxtime = maxtime or self.maxtime

        def create_job(dst, jobgroup=None):
            # build the user command
            user_cmd = " ".join([cmd, self.join(self.LCG_ROOT_VAR, dst)])

            # build the full command
            dst  = self.join(lcg_root, dst)
            _cmd = shlex.split(cmd) + [dst]

            # create the on_status callback
            def on_status(job):
                data = job.__dict__
                if job.status == job.FINISHED:
                    self.remove([job.id])
                    if job.returncode == 0:
                        lines    = [line for line in job.stdout.split("\n") if line]
                        elements = [self.trim(lcg_cutoff, line) for line in lines]
                        data["result"] = elements
                    else:
                        data["stdout"] = job.stdout
                        data["stderr"] = job.stderr
                vispa.remote.send_topic(topic + "status", user_id=user_id, data=data)

            return Job(Job.LS, on_status, _cmd, user_cmd, jobgroup=jobgroup, maxtime=maxtime)

        dst = json.loads(dsts)[0]

        # create the job
        job = create_job(dst)
        if not isinstance(job, Job):
            return job

        vispa.remote.send_topic(topic + "created", user_id=user_id, data=job.__dict__)

        # put it in the queue and process it
        self.queue.append(job)
        self.run([job.id])

    def lcgcp(self, srcs, dsts, mode, cmd, lcg_root, grouped, nprocs, user_id, topic, maxtime=None):
        maxtime = maxtime or self.maxtime

        srcs = json.loads(srcs)
        dsts = json.loads(dsts)
        if len(srcs) != len(dsts):
            return "src and dst lengths do not match"

        def create_job(src, dst, jobgroup=None):
            if mode.startswith("local"):
                user_src = "file:" + src
                src      = "file:" + self.expand(src)
            elif mode.startswith("remote"):
                user_src = self.join("$LCGROOT", src)
                src      = self.join(lcg_root, src)
            if mode.endswith("local"):
                user_dst = "file:" + dst
                dst      = "file:" + self.expand(dst)
            elif mode.endswith("remote"):
                user_dst = self.join("$LCGROOT", dst)
                dst      = self.join(lcg_root, dst)

            user_cmd = " ".join([cmd, user_src, user_dst])
            _cmd     = shlex.split(cmd) + [src, dst]

            # create the on_status callback
            def on_status(job):
                data = job.__dict__
                if job.status == job.FINISHED:
                    self.remove([job.id])
                    if job.returncode != 0:
                        data["stdout"] = job.stdout
                        data["stderr"] = job.stderr
                vispa.remote.send_topic(topic + "status", user_id=user_id, data=data)

            return Job(Job.CP, on_status, _cmd, user_cmd, jobgroup=jobgroup, maxtime=maxtime)

        if grouped:
            # create the on_status callback for the group
            def on_status(jobgroup):
                data = jobgroup.__dict__
                if jobgroup.status == jobgroup.FINISHED:
                    self.remove([job.id])
                    if jobgroup.returncode != 0:
                        data["stdout"] = jobgroup.stdout
                        data["stderr"] = jobgroup.stderr
                vispa.remote.send_topic(topic + "status", user_id=user_id, data=data)

            jobgroup = JobGroup(JobGroup.LS, on_status, nprocs=nprocs, maxtime=maxtime*len(srcs))

            # add the jobs
            for src, dst in zip(srcs, dsts):
                jobgroup.add(create_job(src, dst, jobgroup=jobgroup))

            vispa.remote.send_topic(topic + "created", user_id=user_id, data=jobgroup.__dict__)

            self.queue.append(jobgroup)
        else:
            job = create_job(srcs[0], dsts[0])
            if not isinstance(job, Job):
                return job

            vispa.remote.send_topic(topic + "created", user_id=user_id, data=job.__dict__)

            self.queue.append(job)

    def lcgdel(self, dsts, dirs, cmd, lcg_root, grouped, nprocs, user_id, topic, maxtime=None):
        maxtime = maxtime or self.maxtime

        dsts = json.loads(dsts)
        dirs = json.loads(dirs)
        if len(dsts) != len(dirs):
            return "dst and dir lengths do not match"

        def create_job(dst, dir, jobgroup=None):
            if len(dst.replace("/", "")) == 0:
                return "You cannot delete elements below your LCG root."

            _cmd = shlex.split(cmd)
            if dir:
                _cmd.append("-d")
            user_cmd = " ".join(_cmd + [self.join("$LCGROOT", dst)])
            _cmd.append(self.join(lcg_root, dst))

            # create the on_status callback
            def on_status(job):
                data = job.__dict__
                if job.status == job.FINISHED:
                    self.remove([job.id])
                    if job.returncode != 0:
                        data["stdout"] = job.stdout
                        data["stderr"] = job.stderr
                vispa.remote.send_topic(topic + "status", user_id=user_id, data=data)

            return Job(Job.DEL, on_status, _cmd, user_cmd, jobgroup=jobgroup, maxtime=maxtime)

        if grouped:
            # create the on_status callback for the group
            def on_status(jobgroup):
                data = jobgroup.__dict__
                if jobgroup.status == jobgroup.FINISHED:
                    self.remove([job.id])
                    if jobgroup.returncode != 0:
                        data["stdout"] = jobgroup.stdout
                        data["stderr"] = jobgroup.stderr
                vispa.remote.send_topic(topic + "status", user_id=user_id, data=data)

            jobgroup = JobGroup(JobGroup.DEL, on_status, nprocs=nprocs, maxtime=maxtime*len(dsts))

            for dst, dir in zip(dsts, dirs):
                jobgroup.add(create_job(dst, dir, jobgroup=jobgroup))

            vispa.remote.send_topic(topic + "created", user_id=user_id, data=jobgroup.__dict__)

            self.queue.append(jobgroup)
        else:
            job = create_job(dsts[0], dirs[0])
            if not isinstance(job, Job):
                return job

            vispa.remote.send_topic(topic + "created", user_id=user_id, data=job.__dict__)

            self.queue.append(job)


class AbstractJob(object):

    LS  = "lcg-ls"
    CP  = "lcg-cp"
    DEL = "lcg-del"

    IDLE     = 0
    PENDING  = 1
    RUNNING  = 2
    FINISHED = 3
    STATES   = {
        IDLE       : "idle",
        PENDING    : "pending",
        RUNNING    : "running",
        FINISHED   : "finished"
    }

    def __init__(self, type, on_status, maxtime=60.):
        super(AbstractJob, self).__init__()

        self.type      = type
        self.on_status = on_status
        self.maxtime   = maxtime

        self.id         = str(uuid4())
        self._status    = self.IDLE
        self.started    = None

        self.returncode = None
        self.stdout     = None
        self.stderr     = None

    @property
    def __dict__(self):
        return {
            "type"       : self.type,
            "id"         : self.id,
            "status"     : self.status,
            "status_name": self.STATES[self.status],
            "started"    : self.started
        }

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        if value in self.STATES and self._status != value:
            self._status = value
            self.on_status(self)

    def poll(self):
        raise NotImplementedError

    def communicate(self):
        raise NotImplementedError

    def run(self):
        """
        Returns True on success
        """
        if self.status != self.PENDING:
            return False

        self.status  = self.RUNNING
        self.started = time.time()

        logger.debug("run       " + self.id)

        return True

    def finalize(self):
        """
        Returns True on success
        """
        if self.status == self.FINISHED:
            return True
        elif self.status == self.RUNNING:
            if self.poll() is None:
                # need to terminate?
                if time.time() - self.started > self.maxtime:
                    self.terminate("job maxtime (%s s) exceeded" % self.maxtime)
                    return True
                else:
                    return False
        else:
            return False

        # when we are here, we are running, but poll is not None

        self.returncode          = self.poll()
        self.stdout, self.stderr = self.communicate()

        logger.debug("finalize  " + self.id)
        self.status = self.FINISHED

        logger.info("returncode %s (%s)" % (self.returncode, self.id))

        return True

    def terminate(self, reason=None):
        if self.status not in [self.PENDING, self.RUNNING]:
            return False

        if reason is not None:
            reason = "Terminated: " + reason

        self.returncode = 1
        self.stdout     = reason

        logger.debug("terminate " + self.id + " (%s)" % (reason or "no reason"))
        self.status = self.FINISHED

        return True


class Job(AbstractJob):

    def __init__(self, type, on_status, cmd, user_cmd, jobgroup=None, **kwargs):
        super(Job, self).__init__(type, on_status, **kwargs)

        self.cmd      = cmd
        self.user_cmd = user_cmd
        self.jobgroup = jobgroup

        self.popen = None

    @property
    def __dict__(self):
        data = super(Job, self).__dict__
        data.update({
            "cmd"         : " ".join(self.cmd),
            "user_cmd"    : self.user_cmd,
            "job_group_id": None if self.jobgroup is None else self.jobgroup.id
        })
        return data

    def poll(self):
        if self.popen is None:
            return None
        return self.popen.poll()

    def communicate(self):
        if self.popen is None:
            return None, None
        return self.popen.communicate()

    def run(self):
        if super(Job, self).run() is False:
            return False

        self.popen = Popen(self.cmd, stdout=PIPE, stderr=PIPE)

        return True

    def terminate(self, reason=None):
        if super(Job, self).terminate(reason=reason) is False:
            return False

        if self.popen is not None:
            self.popen.kill()


class JobGroup(AbstractJob):
    pass
    # def __init__(self, type, on_status, nprocs=1, job_maxtime=60., **kwargs):
    #     super(JobGroup, self).__init__(type, on_status, **kwargs)

    #     self.nprocs      = nprocs
    #     self.job_maxtime = job_maxtime

    #     self.maxtime_set = "maxtime" in kwargs

    #     # build jobs
    #     self.jobs = []

    #     # create a pool
    #     self.pool = Pool(nprocs=nprocs, maxtime=job_maxtime, start=False)

    # @property
    # def __dict__(self):
    #     data = super(JobGroup, self).__dict__
    #     data.update({
    #         "nprocs": self.nprocs,
    #         "jobs"  : [job.__dict__ for job in self.jobs]
    #     })
    #     return data

    # def poll(self):
    #     if self.status < self.RUNNING:
    #         return None

    #     codes = [job.poll() for job in self.jobs]

    #     if None in codes:
    #         return None
    #     elif codes.count(0) == len(codes):
    #         return 0
    #     else:
    #         return 1

    # def communicate(self):
    #     pass

    # def run(self):
    #     if super(JobGroup, self).run() is False:
    #         return False

    #     # add all jobs to the pool
    #     for job in self.jobs:
    #         self.pool.add(job)

    #     # adjust the maxtime
    #     if not self.maxtime_set:
    #         self.maxtime = self.job_maxtime * len(self.pool)

    #     # start the pool
    #     self.pool.start()

    #     return True

    # def finalize(self):
    #     if super(JobGroup, self).finalize() is False:
    #         return False

    #     #  finalize all jobs
    #     for job in self.jobs:
    #         job.finalize()

    #     self.pool.stop()
    #     self.callback(self)

    #     return True

    # def terminate(self, reason=None):
    #     if super(JobGroup, self).terminate(reason=reason) is False:
    #         return False

    #     # terminate all jobs
    #     for job in self.jobs:
    #         job.terminate()

    # def add(self, job):
    #     if not isinstance(job, Job):
    #         return False
    #     if job in self.jobs:
    #         return False
    #     self.jobs.append(job)
    #     return True

    # def remove(self, job):
    #     valid_states = [self.IDLE, self.FINISHED]
    #     if self.status not in valid_states:
    #         return False
    #     if job not in self.jobs:
    #         return False
    #     if job.status not in valid_states:
    #         return False
    #     self.jobs.remove(job)
    #     return True


class Pool(object):

    def __init__(self, nprocs=1, delay=.5, maxtime=10800., start=True):
        super(Pool, self).__init__()

        self.nprocs        = nprocs
        self.delay         = delay
        self.maxtime       = maxtime

        self.queue   = []
        self.running = []

        self.scheduler = None
        self._stop     = False

        if start:
            self.start()

    def __del__(self):
        self.stop()

    def __contains__(self, job):
        return job in self.queue or job in self.running

    def __len__(self):
        return len(self.queue) + len(self.running)

    def start(self):
        self.scheduler = Thread(target=self.schedule)
        self.scheduler.daemon = True
        self.scheduler.start()

    def stop(self):
        self._stop = True

    def add(self, job):
        if not isinstance(job, AbstractJob):
            return False
        if job.status != job.IDLE:
            return False
        if job in self:
            return False

        job.status = job.PENDING
        self.queue.append(job)

        return True

    def schedule(self):
        while not self._stop:
            # remove jobs that are not pending
            for job in self.queue:
                if job.status != job.PENDING:
                    self.queue.remove(job)

            # try to start new jobs
            while len(self.running) < self.nprocs and len(self.queue):
                job = self.queue.pop(0)
                if job.run():
                    self.running.append(job)

            # check running jobs
            for job in self.running:
                if job.finalize():
                    self.running.remove(job)
                elif time.time() - job.started > self.maxtime:
                    job.terminate("pool maxtime (%s s) exceeded" % self.maxtime)

            time.sleep(self.delay)
