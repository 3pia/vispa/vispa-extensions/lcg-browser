var LCGBrowserExtension = vispa.Extension.extend({

  init: function() {
    var self = this;
    this._super("lcgbrowser");

    this.addView("center", LCGBrowserView);

    this.addMenuEntry("New LCG Browser", {
      iconClass: "glyphicon glyphicon-globe",
      callback: function(workspaceId) {
        self.createInstance(workspaceId, "center");
      }
    });

    this.setDefaultPreferences("center", {
      lcgRoot: {
        type: "string",
        value: "",
        description: "Your LCG root, e.g. srm://grid-srm.physik.rwth-aachen.de:8443/srm/managerv2?SFN=/pnfs/physik.rwth-aachen.de/cms/store/user/xyz."
      },
      lcgCutoff: {
        type: "string",
        value: "",
        description: "A string to cut from the beginning of paths to match your LCG root, e.g. /pnfs/physik.rwth-aachen.de/cms/store/user/xyz."
      },
      lsCommand: {
        type: "string",
        value: "lcg-ls -b -D srmv2",
        description: "The LCG ls command to use."
      },
      cpCommand: {
        type: "string",
        value: "lcg-cp -b -D srmv2",
        description: "The LCG cp command to use."
      },
      delCommand: {
        type: "string",
        value: "lcg-del -l -b -D srmv2",
        description: "The LCG del command to use."
      },
      widthFraction: {
        type: "integer",
        value: 50,
        range: [0, 100, 1],
        description: "Width fraction in %. 100: only browser, 0: only queue."
      }
    }, {
      title: "LCG Browser"
    });
  }
});


var LCGBrowserView = vispa.ExtensionView.Center.extend({

  init: function(preferences, shortcuts) {
    this._super(preferences, shortcuts);

    this.browser = new LCGBrowser(this);
  },

  setup: function() {
    this._super();

    this.setLabel("LCG Browser");
    this.setIcon("glyphicon glyphicon-globe");

    this.browser.setup();

    return this;
  },

  render: function($node) {
    this.browser.render($node);
    return this;
  },

  applyPreferences: function() {
    this._super();

    // widthFraction
    this.browser.setWidth(this.getPreference("widthFraction"));
  }
});


$(function() {
  vispa.extensions.registerExtension(LCGBrowserExtension);

  vispa.callbacks.on("newLCGBrowser", function(workspaceId, args) {
    var ext = vispa.extensions.getExtension("lcgbrowser");
    ext.createInstance(workspaceId, "center", args);
  });
});
