var LCGBrowser = Emitter.extend({

  init: function(view) {
    this._super();

    this.view = view;

    this.nodes = {
      $main    : null,
      $browser : null,
      $queue   : null
    };

    this.queue = new LCGQueue(this);
  },

  setup: function() {
    var self = this;

    if (!this.view.getPreference("lcgRoot")) {
      this.queryLCGPrefs();
    }

    // create jobs and job groups
    this.view.onSocket("created", this.createJob.bind(this));

    // re-emit status events
    this.view.onSocket("status", function(data) {
      // get the job, deep is true
      var job = self.queue.get(data.id, true);
      if (!(job instanceof LCGAbstractJob)) {
        return;
      }
      delete data.id;
      job.emit("status", data);
    });
  },

  render: function($node) {
    var self = this;

    this.view.getTemplate("html/main.html").done(function(tmpl) {
      var $main = $(tmpl).appendTo($node);

      var $browser = $main.find(".browser");
      var $queue   = $main.find(".queue");

      // resizable
      $queue.resizable({
        handles: "w,e",
        start: function() {
          var mainWidth  = $main.width();
          $queue.resizable("option", "grid", [mainWidth * 0.01, 1]);
        },
        resize: function() {
          var queueWidth = $queue.width();
          var mainWidth  = $main.width();
          $browser.css({
            left : 0,
            width: mainWidth - queueWidth
          });
          $queue.css({
            left : mainWidth - queueWidth,
            width: queueWidth
          });
        },
        stop: function() {
          var queueWidth = $queue.width();
          var mainWidth  = $main.width();
          var frac = Math.round((mainWidth - queueWidth) / mainWidth * 100.0);

          // tell the preferences
          self.view.setPreference("widthFraction", frac);
          self.view.pushPreferences();
        }
      });

      // enable tooltips
      $(".title", $main).tooltip({ delay: { "show": 500, "hide": 0 } });

      // store nodes
      self.nodes.$main    = $main;
      self.nodes.$browser = $browser;
      self.nodes.$queue   = $queue;

      // test
      // $("<button class='btn btn-success'>ls</button>").appendTo($browser.find(".inner")).click(function(event) {
      //   var data = { dsts: JSON.stringify(["/"]) };
      //   self.view.GET("lcgls", data);
      // });

      // $("<button class='btn btn-success'>cp</button>").appendTo($browser.find(".inner")).click(function(event) {
      //   var data = { srcs: JSON.stringify(["~/somefile.txt"]), dsts: JSON.stringify(["somefile.txt"]), mode: "local2remote" };
      //   self.view.POST("lcgcp", data);
      // });

      // $("<button class='btn btn-success'>del</button>").appendTo($browser.find(".inner")).click(function(event) {
      //   var data = { dsts: JSON.stringify(["somefile.txt"]), dirs: JSON.stringify([false]) };
      //   self.view.POST("lcgdel", data);
      // });
      // test end

      // initially, load the lcg root node
      self.view.getTemplate("html/node.html").done(function(tmpl) {
        var $node = $(tmpl).appendTo($browser.find(".inner"))
          .render({ title: "$LCG_ROOT" });

        $node.find(".handle").first()
          .toggleClass("glyphicon-none", false)
          .toggleClass("glyphicon-chevron-down", true)
        $node.find(".nodes").first().show();

        // test
        [1, 2, 3, 4, 5].forEach(function(i) {
          var title = "Data_" + i + ".pxlio";
          $(tmpl).appendTo($node.find(".nodes").first()).render({ title: title });
        });

        $(tmpl).appendTo($node.find(".nodes").first()).render({ title: "somefolder" })
          .find(".handle").first()
          .toggleClass("glyphicon-none", false)
          .toggleClass("glyphicon-chevron-right", true)
      });

      // load the queue
      self.view.GET("queue").done(function(queue) {
        queue.forEach(self.createJob.bind(self));
      });

      // setup the queue
      self.queue.setup();

      // apply preferences
      self.view.applyPreferences();
    });
  },

  queryLCGPrefs: function() {
    var self = this;

    var promptCut = function() {
      var msg = "You may also want to set a LCG path cutoff. This is a string that is cut from "
        + "returned paths in order to match your LCG root."
        self.view.prompt(msg, function(input) {
          if (input) {
            self.view.setPreference("lcgCutoff", input);
            self.view.pushPreferences();
          }
        });
    };

    var promptRoot = function() {
      var msg = "Your LCG root is empty. Please set it to ensure proper app behavior."
      self.view.prompt(msg, function(input) {
        if (!input) return promptRoot();

        self.view.setPreference("lcgRoot", input);
        self.view.pushPreferences();

        if (!self.view.getPreference("lcgCutoff")) {
          promptCut();
        }
      });
    };

    promptRoot();

    return this;
  },

  setWidth: function(widthFraction) {
    if (!this.nodes.$main) {
      return this;
    }

    this.nodes.$browser.removeAttr("style").css({
      left : 0,
      width: widthFraction + "%"
    });
    this.nodes.$queue.removeAttr("style").css({
      right: 0,
      width: (100 - widthFraction) + "%"
    });

    // resize all jobs
    this.queue.queue.forEach(function(job) {
      job.resize();
    });

    return this;
  },

  createJob: function(data) {
    if (!this.queue.has(data.id, true)) {
      var cls = data.group ? LCGJobGroup : LCGJob;
      this.queue.put(new cls(this, data));
    }
    return this;
  }
});


var LCGQueue = Emitter.extend({

  init: function(browser) {
    this._super();

    this.browser = browser;
    this.view      = browser.view;

    this.queue = [];

    this.nodes = {
      $checkbox: null,
      $run     : null,
      $remove  : null
    };
  },

  setup: function() {
    var self = this;

    // enlarge the selector area
    this.browser.nodes.$queue.find(".header > .select").click(function(event) {
      if (!event.originalEvent.checkboxClicked) {
        self.selectAll(!self.nodes.$checkbox.get(0).checked);
      }
    });
    this.browser.nodes.$queue.find(".header > .select > input").click(function(event) {
      event.originalEvent.checkboxClicked = true;
      self.selectAll(this.checked);
    });

    // store some nodes
    this.nodes.$checkbox = this.browser.nodes.$queue.find(".header > .select > input")
    this.nodes.$run      = this.browser.nodes.$queue.find(".header > .run > button");
    this.nodes.$remove   = this.browser.nodes.$queue.find(".header > .remove > button");

    // actions
    this.nodes.$run.click(function(event) {
      var jobIds = self.queue.map(function(job) {
        return job.selected() ? job.id : null;
      }).filter(function(jobId) {
        return !!jobId;
      });
      self.process(jobIds);
      self.selectAll(false);
    });

    this.nodes.$remove.click(function(event) {
      var jobIds = self.queue.map(function(job) {
        return job.selected() ? job.id : null;
      }).filter(function(jobId) {
        return !!jobId;
      });
      self.remove(jobIds);
      self.selectAll(false);
    });

    return this;
  },

  get: function(jobId, deep) {
    deep = deep === undefined ? false : deep;

    if (jobId instanceof LCGAbstractJob) {
      jobId = jobId.id;
    }

    var lookup = this.queue.concat();
    while (lookup.length) {
      var job = lookup.shift();
      if (job.id == jobId) {
        return job;
      } else if (deep && job instanceof LCGJobGroup) {
        lookup = lookup.concat(job.jobs);
      }
    }
    return null;
  },

  has: function(jobId, deep) {
    return !!this.get(jobId, deep);
  },

  put: function(job) {
    var self = this;

    if (!(job instanceof LCGAbstractJob) || this.has(job, true)) {
      return this;
    }

    this.queue.push(job);
    job.setup();

    // listen
    job.on("select", function(state) {
      // equal states for all jobs?
      var states = self.queue.map(function(job) {
        return job.selected();
      });

      var oneSelected = !!~states.indexOf(true);
      self.nodes.$run.toggleClass("disabled", !oneSelected);
      self.nodes.$remove.toggleClass("disabled", !oneSelected);

      if (~states.indexOf(true) && ~states.indexOf(false)) {
        return;
      }
      self.selectAll(state);
    });

    return this;
  },

  remove: function(jobIds, tellServer) {
    var self = this;

    tellServer = tellServer === undefined ? true : tellServer;
    jobIds     = jobIds.filter(function(jobId) {
      var job = self.get(jobId);
      return job && (job.status == LCG_IDLE || job.status == LCG_FINISHED);
    });

    if (!jobIds.length) {
      return this;
    }

    var remove = function(jobIds) {
      jobIds.forEach(function(jobId) {
        var job = self.get(jobId);
        if (job instanceof LCGAbstractJob) {
          var idx = self.queue.indexOf(job);
          if (~idx) {
            self.queue.splice(idx, 1);
            job.destroy();
          }
        }
      });
    };

    if (tellServer) {
      this.view.POST("remove", {ids: JSON.stringify(jobIds)}).done(function(removed) {
        // remove from queue and destroy
        remove(removed);
        var notRemoved = jobIds.filter(function(jobId) {
          return !~removed.indexOf(jobId);
        });
        if (notRemoved.length) {
          var jobs = notRemoved.map(function(jobId) {
            return self.get(jobId);
          });
          self.view.getTemplate("html/error.html").done(function(tmpl) {
            self.view.alert($(tmpl).render({ job: jobs }, {
              message: {
                html: function() { return "The following jobs could not be removed."; }
              }
            }), {
              header: "<i class='glyphicon glyphicon-globe'></i> Error in command removal"
            });
          });
        }
      });
    } else {
      remove(jobIds);
    }

    return this;
  },

  process: function(jobIds) {
    var self = this;

    jobIds = jobIds.filter(function(jobId) {
      var job = self.get(jobId);
      return job && job.status == LCG_IDLE;
    });

    if (jobIds.length) {
      this.view.POST("run", { ids: JSON.stringify(jobIds) }).done(function(started) {
        var notStarted = jobIds.filter(function(jobId) {
          return !~started.indexOf(jobId);
        });
        if (notStarted.length) {
          var jobs = notStarted.map(function(jobId) {
            return self.get(jobId);
          });
          self.view.getTemplate("html/error.html").done(function(tmpl) {
            self.view.alert($(tmpl).render({ job: jobs }, {
              message: {
                html: function() { return "The following jobs could not be started."; }
              }
            }), {
              header: "<i class='glyphicon glyphicon-globe'></i> Error in job processing"
            });
          });
        }
      });
    }
    return this;
  },

  terminate: function(job) {
    var job = this.get(job);
    if (!(job instanceof LCGAbstractJob)) {
      return this;
    }

    if (job.status != LCG_PENDING && job.status != LCG_RUNNING) {
      return this;
    }

    this.view.POST("terminate", { ids: JSON.stringify([job.id]) });

    return this;
  },

  selectAll: function(state) {
    if (!this.browser.nodes.$queue) {
      return this;
    }

    state = state === undefined ? true : state;

    this.nodes.$checkbox.prop("checked", state);

    this.queue.forEach(function(job) {
      job.select(state);
    });

    return this;
  }
});


// states
var LCG_IDLE        = 0;
var LCG_PENDING     = 1;
var LCG_RUNNING     = 2;
var LCG_FINISHED    = 3;

// types
var LCG_LS  = "lcg-ls";
var LCG_CP  = "lcg-cp";
var LCG_DEL = "lcg-del";


var LCGAbstractJob = Emitter.extend({
  /*
    External messages: status
    Internal messages: selected
  */

  init: function(browser, data) {
    var self = this;
    this._super();

    this.browser  = browser;
    this.queue      = browser.queue;
    this.view       = browser.view;

    this.type       = data.type;
    this.id         = data.id;
    this.started    = null;
    this.status     = data.status;
    this.statusName = data.status_name;

    this.result = null;
    this.stdout = null;
    this.stderr = null;

    // listeners for external messages
    this.on("status", function(data) {
      self.status     = data.status;
      self.statusName = data.status_name;
      self.applyStatus();

      if (self.status == LCG_FINISHED) {
        if ("stderr" in data) {
          self.stdout = data.stdout;
          self.stderr = data.stderr;
          self.applyError();
        } else {
          self.result = data.result;
          self.applyResult();
        }
      }
    });
  },

  setup: function() {
    throw new Error("not implemented");
  },

  destroy: function() {
    throw new Error("not implemented");
  },

  resize: function() {
    throw new Error("not implemented");
  },

  run: function() {
    if (self.status != LCG_IDLE) {
      return this;
    }
    this.queue.process([this.id]);
    return this;
  },

  terminate: function() {
    this.queue.terminate([this.id]);
    return this;
  },

  remove: function(tellServer) {
    this.queue.remove([this.id], tellServer);
    return this;
  },

  applyStatus: function() {
    console.log("status", this.status, this.statusName);
    return this;
  },

  applyResult: function() {
    console.log("result", this.result);
    this.remove(false);
    return this;
  },

  applyError: function() {
    var self = this;

    this.remove(false);

    var msg = [this.stdout, this.stderr].filter(function(s) {
      return !!s;
    }).join("<br /><br />");

    msg = msg || "An unknown error occured while processing a '" + this.type + "' command. "
      + "You maybe need to setup your proxy via <kbd>voms-proxy-init</kbd> in the terminal first.";

    this.view.getTemplate("html/error.html").done(function(tmpl) {
      self.view.alert($(tmpl).render({job: self}, {
        message: {
          html: function() { return msg; }
        }
      }), {
        header: "<i class='glyphicon glyphicon-globe'></i> Error in <kbd>" + self.type + "</kbd>"
      });
    });

    return this;
  }
});


var LCGJob = LCGAbstractJob.extend({

  init: function(browser, data) {
    this._super(browser, data);

    this.cmd     = data.cmd;
    this.userCmd = data.user_cmd;
    this.group   = data.group;

    this.nodes = {
      $main     : null,
      $checkbox : null,
      $run      : null,
      $terminate: null,
      $remove   : null,
      $status   : null
    };
  },

  setup: function() {
    var self = this;

    // depends on the type
    if (self.type == LCG_LS) {
      // nothing
      return this;
    } else if (self.type == LCG_CP || self.type == LCG_DEL) {
      this.view.getTemplate("html/job.html").done(function(tmpl) {
        self.nodes.$main = $(tmpl)
          .render(self)
          .appendTo(self.browser.nodes.$queue.find(".inner"));

        // store some nodes
        self.nodes.$checkbox  = self.nodes.$main.find(".select > input");
        self.nodes.$run       = $(self.nodes.$main.find(".run > button").get(0));
        self.nodes.$terminate = $(self.nodes.$main.find(".run > button").get(1));
        self.nodes.$remove    = self.nodes.$main.find(".remove > button");
        self.nodes.$status    = self.nodes.$main.find(".status");

        // colorize the label dependent on the type
        var className = self.type == LCG_CP ? "label-info" : "label-danger";
        self.nodes.$main.find(".type > span").addClass(className);

        // enlarge the selector area
        self.nodes.$main.find(".select").click(function(event) {
          if (!event.originalEvent.checkboxClicked) {
            self.select(!self.nodes.$checkbox.get(0).checked);
          }
        });
        self.nodes.$main.find(".select > input").click(function(event) {
          event.originalEvent.checkboxClicked = true;
          self.select(this.checked, true);
        });

        // ellipses
        self.nodes.$main.find(".id").ellipsis2({ position: "middle" });
        self.nodes.$main.find(".cmd").ellipsis2({ position: "front" });

        // tooltips
        self.nodes.$main.find(".id").tooltip({
          placement: "bottom",
          delay: { "show": 500, "hide": 0 },
          title: function() {
            return self.id;
          }
        });
        self.nodes.$main.find(".cmd").tooltip({
          placement: "bottom",
          delay: { "show": 500, "hide": 0 },
          title: function() {
            return self.cmd;
          }
        });

        self.nodes.$run.click(function(event) {
          self.run();
        });
        self.nodes.$terminate.click(function(event) {
          self.terminate();
        });
        self.nodes.$remove.click(function(event) {
          self.remove();
        });

        // apply the status
        self.applyStatus();
      });

      return this;
    }
  },

  destroy: function() {
    // depends in the type
    if (this.type == LCG_LS) {
      // nothing
      return this;
    } else if (this.type == LCG_CP || this.type == LCG_DEL) {
      this.nodes.$main.remove();
      this.nodes.$main = null;

      return this;
    }
  },

  resize: function() {
    if (this.nodes.$main) {
      this.nodes.$main.find(".cmd").ellipsis2({ position: "front" });
    }
    return this;
  },

  select: function(state, force) {
    if (!this.nodes.$main) {
      return this;
    }

    state = state === undefined ? true : state;

    if (!force && this.selected() == state) {
      return this;
    }

    this.nodes.$checkbox.prop("checked", state);

    this.emit("select", state);

    return this;
  },

  selected: function() {
    if (!this.nodes.$main) {
      return false;
    }
    return this.nodes.$main.find(".select > input").get(0).checked;
  },

  applyStatus: function() {
    this._super();

    if (!this.nodes.$status) {
      return this;
    }

    this.nodes.$status.text(this.statusName);

    if (this.status == LCG_IDLE) {
      this.nodes.$run.show().toggleClass("disabled", false);
      this.nodes.$terminate.hide().toggleClass("disabled", true);
      this.nodes.$remove.toggleClass("disabled", false);
    } else if (this.status == LCG_PENDING || this.status == LCG_RUNNING) {
      this.nodes.$run.hide().toggleClass("disabled", true);
      this.nodes.$terminate.show().toggleClass("disabled", false);
      this.nodes.$remove.toggleClass("disabled", true);
    } else if (this.status == LCG_FINISHED) {
      this.nodes.$run.show().toggleClass("disabled", true);
      this.nodes.$terminate.hide().toggleClass("disabled", true);
      this.nodes.$remove.toggleClass("disabled", true);
    }

    return this;
  },

  applyResult: function() {
    this._super();
    return this;
  },

  applyError: function(error) {
    this._super();
    return this;
  }
});


var LCGJobGroup = LCGAbstractJob.extend({

  init: function(data) {
    this._super(data);

    this.jobs = [];

    console.log("created JobGroup", this);
  },

  add: function(job) {
    if (!(job instanceof LCGJob)) {
      return false;
    }
    this.jobs.push(job);
    return true;
  }
});
