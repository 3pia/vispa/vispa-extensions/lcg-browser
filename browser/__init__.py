# -*- coding: utf-8 -*-

import cherrypy
from vispa.server import AbstractExtension
from vispa.controller import AbstractController
from vispa.models.preference import ExtensionPreference
from vispa import MessageException
import logging

logger = logging.getLogger(__name__)


class LCGBrowserExtension(AbstractExtension):

    def name(self):
        return "lcgbrowser"

    def dependencies(self):
        return []

    def setup(self):
        self.add_controller(LCGBrowserController(self))
        self.add_js("js/extension.js")
        self.add_js("js/browser.js")
        self.add_css("css/styles.css")
        self.add_workspace_directoy()


class LCGBrowserController(AbstractController):

    def __init__(self, extension):
        AbstractController.__init__(self)

        self.extension = extension
        self.prefs_key = "lcgbrowser-center"

    def rpc(self):
        return self.extension.get_workspace_instance("LCGBrowserRpc")

    def get_lcg_paths(self):
        db      = cherrypy.request.db
        user_id = cherrypy.request.user.id

        prefs = ExtensionPreference.get_data_by_user_id(db, user_id, self.prefs_key, True)

        lcg_root   = prefs.get("lcgRoot", None)
        lcg_cutoff = prefs.get("lcgCutoff", "")

        return lcg_root, lcg_cutoff

    def get_lcg_commands(self):
        db      = cherrypy.request.db
        user_id = cherrypy.request.user.id

        prefs = ExtensionPreference.get_data_by_user_id(db, user_id, self.prefs_key, True)

        cmd_ls  = prefs.get("lsCommand", "lcg-ls -b -D srmv2")
        cmd_cp  = prefs.get("cpCommand", "lcg-cp -b -D srmv2")
        cmd_del = prefs.get("delCommand", "lcg-del -l -b -D srmv2")

        return cmd_ls, cmd_cp, cmd_del

    @cherrypy.expose
    @cherrypy.tools.allow(methods=["GET"])
    @cherrypy.tools.ajax(encoded=True)
    def queue(self):
        rpc = self.rpc()
        return rpc.get_queue()

    @cherrypy.expose
    @cherrypy.tools.allow(methods=["POST"])
    @cherrypy.tools.ajax(encoded=True)
    def remove(self, ids):
        rpc = self.rpc()
        return rpc.remove(ids)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=["POST"])
    @cherrypy.tools.ajax(encoded=True)
    def run(self, ids):
        rpc = self.rpc()
        return rpc.run(ids)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=["POST"])
    @cherrypy.tools.ajax()
    def terminate(self, ids):
        rpc = self.rpc()
        rpc.terminate(ids)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=["GET"])
    @cherrypy.tools.ajax()
    def lcgls(self, dsts):
        user_id = cherrypy.request.user.id
        topic   = self.extension.create_topic()
        rpc     = self.rpc()
        cmd     = self.get_lcg_commands()[0]

        lcg_root, lcg_cutoff = self.get_lcg_paths()
        if not lcg_root:
            raise MessageException("LCG root is empty! Please set it in your preferences.")

        err = rpc.lcgls(dsts, cmd, lcg_root, lcg_cutoff, False, 1, user_id, topic)
        if err:
            raise MessageException(err)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=["POST"])
    @cherrypy.tools.ajax()
    def lcgcp(self, srcs, dsts, mode, grouped=False, nprocs=1):
        # check the mode
        modes = ["remote2remote", "local2local", "remote2local", "local2remote"]
        if mode not in modes:
            raise MessageException("Mode %s is unknown." % mode)

        user_id = cherrypy.request.user.id
        topic   = self.extension.create_topic()
        rpc     = self.rpc()
        cmd     = self.get_lcg_commands()[1]

        lcg_root = self.get_lcg_paths()[0]
        if not lcg_root:
            raise MessageException("LCG root is empty! Please set it in your preferences.")

        err = rpc.lcgcp(srcs, dsts, mode, cmd, lcg_root, grouped, nprocs, user_id, topic)
        if err:
            raise MessageException(err)

    @cherrypy.expose
    @cherrypy.tools.allow(methods=["POST"])
    @cherrypy.tools.ajax()
    def lcgdel(self, dsts, dirs, grouped=False, nprocs=1):
        user_id = cherrypy.request.user.id
        topic   = self.extension.create_topic()
        rpc     = self.rpc()
        cmd     = self.get_lcg_commands()[2]

        lcg_root = self.get_lcg_paths()[0]
        if not lcg_root:
            raise MessageException("LCG root is empty! Please set it in your preferences.")

        err = rpc.lcgdel(dsts, dirs, cmd, lcg_root, grouped, nprocs, user_id, topic)
        if err:
            raise MessageException(err)
